<?php

namespace Tests\Feature;

use App\Models\Price;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Tests\TestCase;

class PricesControllerTest extends TestCase
{
    public function testGetPrices(): void
    {
//        $page = rand(1, 10);
//        $response = $this->get("/api/prices?page=$page");
        $response = $this->get("/api/prices");

//        $data = $response->json();

        $response->assertStatus(200);
        $response->assertJsonStructure([
           0 => ['guid', 'price', 'product'],
//            1 => ['guid', 'price', 'product']
        ]);
    }

    public function testUpdatePrices(): void
    {
        /** @var Product $product */
        $product = Product::query()->inRandomOrder()->first();
        $product->load('prices');
        $randPrices = $product->prices()->inRandomOrder()->limit(5)->get();

        $url = "/api/prices/$product->guid";
        $data = [];
        foreach ($randPrices as $price) {
            $data[] = [
                'guid' => $price->guid,
                'price' => rand(1, 500000)
            ];
        }

        $response = $this->put($url, ['prices' => $data]);

        $response->assertStatus(200);
        /** @var Collection $prices */
        $prices = Price::whereIn('guid', Arr::pluck($data, 'guid'))->get();
        foreach ($data as $p) {
            $price = $prices->where('guid', $p['guid'])->first();
            $this->assertEquals($p['price'], $price->price);
        }
    }
}
