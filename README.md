# Sellerexpert test task

## Project setup
1. Create database and user
2. Copy .env
    ```shell
   cp .env.example .env
   ```
3. Set .env vars
   ```env
   DB_DATABASE=sellerexpert_task
   DB_USERNAME=sellerexpert_task
   DB_PASSWORD=sellerexpert_task
    ```
4. Install deps
    ```shell
    composer i
    ```
5. Generate key
    ```shell
    php artisan key:generate
    ```
6. Migrations and seeds
   ```shell
   php artisan migrate
   php artisan db:seed
    ```
7. Run tests
   ```shell
   vendor/bin/phpunit
    ```
