<?php

namespace App\Services;

use App\Models\Product;

class PriceService
{
    public function update(string $productGuid, array $prices): void
    {
        $product = Product::find($productGuid);
        foreach ($prices as $p) {
            $price = $product->prices
                ->where('guid', $p['guid'])
                ->first();
            if (!$price) {
                continue;
            }
            $price->price = $p['price'];
            $price->save();
        }
    }
}
