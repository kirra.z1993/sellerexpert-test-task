<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;
    use UsesUuid;

    public $timestamps = false;
    protected $primaryKey = 'guid';

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class, 'product_guid', 'guid');
    }
}
