<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Price extends Model
{
    use HasFactory;
    use UsesUuid;

    public $timestamps = false;
    protected $primaryKey = 'guid';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_guid', 'guid');
    }
}
