<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetPricesRequest;
use App\Http\Requests\UpdatePricesRequest;
use App\Http\Resources\PriceResource;
use App\Models\Price;
use App\Models\Product;
use App\Repositories\PriceRepository;
use App\Services\PriceService;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PricesController extends Controller
{
    public function index(GetPricesRequest $request, PriceRepository $repository): \Illuminate\Http\JsonResponse
    {
        return response()->json(PriceResource::collection($repository->get()));
    }

    public function update(string $productGuid, UpdatePricesRequest $request, PriceService $service): \Illuminate\Http\JsonResponse
    {
        $service->update($productGuid, $request->prices ?? []);
        return response()->json()->setStatusCode(ResponseAlias::HTTP_OK);
    }
}
