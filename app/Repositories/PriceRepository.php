<?php

namespace App\Repositories;

use App\Models\Price;
use Illuminate\Pagination\LengthAwarePaginator;

class PriceRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function get(): LengthAwarePaginator
    {
        // todo: pagination or not?
        return Price::paginate(200);
    }
}
